package com;

import com.model.CarClass;
import com.model.Price;
import com.model.Safety;
import com.model.Size;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

public class DataRepairer {

    private Random generator = new Random();

    public List<CarBean> repair(final List<CarBean> carBeans, Properties badRecords) {

        for (Map.Entry<?, ?> entry: badRecords.entrySet()) {
            int key = (Integer) entry.getKey();
            List<CarField> values = (List) entry.getValue();
            for (CarField value: values) {
                switch (value) {
                    case BOOT: carBeans.get(key - 1).setBootSize(getRandomSize());
                    break;
                    case COST: carBeans.get(key - 1).setCostOfUse(getRandomPrice());
                    break;
                    case CLASS: carBeans.get(key - 1).setCarClass(getRandomCarClass());
                    break;
                    case CAPACITY: carBeans.get(key - 1).setCapacity(getRandomCapacity());
                    break;
                    case SAFETY: carBeans.get(key - 1).setSafety(getRandomSafety());
                    break;
                    case PRICE: carBeans.get(key - 1).setPrice(getRandomPrice());
                    break;
                    case NUMBER_OF_DOOR: carBeans.get(key - 1).setNumberOfDoor(getRandomNumberOfDoor());
                    break;
                }
            }
        }
        return carBeans;

    }
    private String getRandomNumberOfDoor() {
        int number = generator.nextInt(5 - 1) + 2;
        if(number == 5) {
            return String.valueOf(number) + "more";
        }
        return String.valueOf(number);
    }

    private String getRandomCapacity() {
        int number =  generator.nextInt(5 - 1) + 2;
        while(number == 3) {
            number =  generator.nextInt(5 - 1) + 2;
        }
        if(number == 5) {
            return "more";
        }
        return String.valueOf(number);
    }

    private String getRandomSafety() {
        int number =  generator.nextInt(3) + 1;
        return Safety.fieldByValue(number).name();
    }

    private String getRandomSize() {
        int number =  generator.nextInt(3) + 1;
        return Size.fieldByValue(number).name();
    }

    private String getRandomCarClass() {
        int number =  generator.nextInt(4) + 1;
        return CarClass.fieldByValue(number).name();
    }


    private String getRandomPrice() {
        int number =  generator.nextInt(4) + 1;
        return Price.fieldByValue(number).name();
    }

}
