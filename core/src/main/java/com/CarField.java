package com;

public enum CarField {
    CLASS(1),
    BOOT(2),
    CAPACITY(3),
    COST(4),
    PRICE(5),
    SAFETY(6),
    NUMBER_OF_DOOR(7);

    int value;

    CarField(int p) {
        value = p;
    }

    public int getValue() {
        return value;
    }


    public static CarField fieldByValue(int value) {
        for (CarField v : values()) {
            if (v.value == value) {
                return v;
            }
        }
        throw new IllegalArgumentException(
                "No enum const " + CarField.class + "@value." + value);
    }
}
