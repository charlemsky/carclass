package com;

import java.io.Serializable;

public class CarBean implements Serializable {

    private String carClass;
    private String capacity;
    private String numberOfDoor;
    private String price;
    private String safety;
    private String bootSize;
    private String costOfUse;

    public CarBean() {
    }

    public String getCarClass() {
        return carClass;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getNumberOfDoor() {
        return numberOfDoor;
    }

    public void setNumberOfDoor(String numberOfDoor) {
        this.numberOfDoor = numberOfDoor;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSafety() {
        return safety;
    }

    public void setSafety(String safety) {
        this.safety = safety;
    }

    public String getBootSize() {
        return bootSize;
    }

    public void setBootSize(String bootSize) {
        this.bootSize = bootSize;
    }

    public String getCostOfUse() {
        return costOfUse;
    }

    public void setCostOfUse(String costOfUse) {
        this.costOfUse = costOfUse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarBean carBean = (CarBean) o;

        if (carClass != null ? !carClass.equals(carBean.carClass) : carBean.carClass != null) return false;
        if (capacity != null ? !capacity.equals(carBean.capacity) : carBean.capacity != null) return false;
        if (numberOfDoor != null ? !numberOfDoor.equals(carBean.numberOfDoor) : carBean.numberOfDoor != null)
            return false;
        if (price != null ? !price.equals(carBean.price) : carBean.price != null) return false;
        if (safety != null ? !safety.equals(carBean.safety) : carBean.safety != null) return false;
        if (bootSize != null ? !bootSize.equals(carBean.bootSize) : carBean.bootSize != null) return false;
        return costOfUse != null ? costOfUse.equals(carBean.costOfUse) : carBean.costOfUse == null;
    }

    @Override
    public int hashCode() {
        int result = carClass != null ? carClass.hashCode() : 0;
        result = 31 * result + (capacity != null ? capacity.hashCode() : 0);
        result = 31 * result + (numberOfDoor != null ? numberOfDoor.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (safety != null ? safety.hashCode() : 0);
        result = 31 * result + (bootSize != null ? bootSize.hashCode() : 0);
        result = 31 * result + (costOfUse != null ? costOfUse.hashCode() : 0);
        return result;
    }
}
