package com.validation;

import com.CarBean;
import com.CarField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class CarValidator implements IValidator {

    private Properties invalidRecords = new Properties();

    private List<CarBean> elements;

    public CarValidator(List<CarBean> elements) {
        this.elements = elements;
    }

    public boolean validate() {
        boolean isValid = true;
        for (int i = 0; i < elements.size(); i++) {
            isValid = validElement(elements.get(i), i);
        }
        return isValid;
    }

    private boolean validElement(CarBean carBean, int number) {
        boolean isValid = true;
        List<CarField> validElementOfRecord = new ArrayList<CarField>();

        if(!carClassValid(carBean)) {
            isValid = false;
            validElementOfRecord.add(CarField.CLASS);
        }
        if(!bootSizeValid(carBean)) {
            isValid = false;
            validElementOfRecord.add(CarField.BOOT);
        }
        if(!costOfUseValid(carBean)) {
            isValid = false;
            validElementOfRecord.add(CarField.COST);
        }
        if(!priceValid(carBean)) {
            isValid = false;
            validElementOfRecord.add(CarField.PRICE);
        }
        if(!safetyValid(carBean)) {
            isValid = false;
            validElementOfRecord.add(CarField.SAFETY);
        }
        if(!numberOfDoorValid(carBean)) {
            isValid = false;
            validElementOfRecord.add(CarField.NUMBER_OF_DOOR);
        }
        if(!capacityValid(carBean)) {
            isValid = false;
            validElementOfRecord.add(CarField.CAPACITY);
        }
        if(validElementOfRecord.size() > 0) {
            invalidRecords.put(number + 1, validElementOfRecord);
        }
        return isValid;

    }

    public Properties getInvalidRecords() {
        return invalidRecords;
    }

    private boolean carClassValid(CarBean carBean) {
        List<String> permittedValue = Arrays.asList( "unacc", "acc", "good", "vgood");
        return permittedValue.contains(carBean.getCarClass());
    }

    private boolean bootSizeValid(CarBean carBean) {
        List<String> permittedValue = Arrays.asList("small", "med", "big");
        return permittedValue.contains(carBean.getBootSize());
    }

    private boolean costOfUseValid(CarBean carBean) {
        List<String> permittedValue = Arrays.asList( "vhigh", "high", "med", "low");
        return permittedValue.contains(carBean.getCostOfUse());
    }

    private boolean priceValid(CarBean carBean) {
        List<String> permittedValue = Arrays.asList( "vhigh", "high", "med", "low");
        return permittedValue.contains(carBean.getPrice());
    }

    private boolean safetyValid(CarBean carBean) {
        List<String> permittedValue = Arrays.asList("high", "med", "low");
        return permittedValue.contains(carBean.getSafety());
    }

    private boolean numberOfDoorValid(CarBean carBean) {
        List<String> permittedValue = Arrays.asList( "2", "3", "4", "5more");
        return permittedValue.contains(carBean.getNumberOfDoor());
    }

    private boolean capacityValid(CarBean carBean) {
        List<String> permittedValue = Arrays.asList( "2", "4", "more");
        return permittedValue.contains(carBean.getCapacity());
    }
}
