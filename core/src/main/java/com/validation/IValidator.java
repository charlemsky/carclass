package com.validation;

import java.util.Properties;

public interface IValidator {
    boolean validate();
    Properties getInvalidRecords();
}
