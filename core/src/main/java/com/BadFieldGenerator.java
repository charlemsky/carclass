package com;

import java.util.List;
import java.util.Random;


public class BadFieldGenerator {


    public List<CarBean> generate(
            final List<CarBean> carBeans, int numberOfBadRecord) {
        CarBean car;
        for (int i = 0; i < numberOfBadRecord; i++) {
            int badRecord = getRandomRecordNumber(carBeans);
            car = carBeans.get(badRecord);
            carBeans.remove(car);
            switch (getRandomCarField()) {
                case BOOT:
                    car.setBootSize("wrongBootSize");
                    break;
                case CLASS:
                    car.setCarClass("wrongClass");
                    break;
                case NUMBER_OF_DOOR:
                    car.setNumberOfDoor("15");
                    break;
                case PRICE:
                    car.setPrice("124521");
                    break;
                case SAFETY:
                    car.setSafety("wrong value");
                    break;
                case COST:
                    car.setCostOfUse("wrong cost");
                    break;
                case CAPACITY:
                    car.setCapacity("wrong capacity");
                    break;
            }
            carBeans.add(car);
        }
        return carBeans;
    }

    CarField getRandomCarField() {
        Random generator = new Random();
        int i = generator.nextInt(7) + 1;
        return CarField.fieldByValue(i);
    }

    int getRandomRecordNumber(List<CarBean> carBeans) {
        Random generator = new Random();
        int i = generator.nextInt(carBeans.size());
        return i;
    }
}
