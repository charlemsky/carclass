package com.model;

import java.math.BigDecimal;

public enum CarClass{
    unacc(1), acc(2), good(3), vgood(4);

    int value;

    CarClass(int p) {
        value = p;
    }

    public int getValue() {
        return value;
    }


    public static CarClass fieldByValue(int value) {
        for (CarClass v : values()) {
            if (v.value == value) {
                return v;
            }
        }
        throw new IllegalArgumentException(
                "No enum const " + CarClass.class + "@value." + value);
    }
}
