package com.model;

import javax.xml.bind.JAXBContext;
import java.io.Serializable;
import java.math.BigDecimal;

public class Car implements Serializable {
    private CarClass carClass;
    private Size bootSize;
    private Price costOfUse;
    private Price price;
    private Safety safety;
    private Integer numberOfDoor;
    private Integer capacity;

    public Car(Builder builder) {
        this.capacity = builder.capacity;
        this.bootSize = builder.bootSize;
        this.costOfUse = builder.cost;
        this.price = builder.price;
        this.carClass = builder.carClass;
        this.numberOfDoor = builder.numberOfDoor;
        this.safety = builder.safety;
    }

    public CarClass getCarClass() {
        return carClass;
    }

    public Size getBootSize() {
        return bootSize;
    }

    public Price getCostOfUse() {
        return costOfUse;
    }

    public Price getPrice() {
        return price;
    }

    public Safety getSafety() {
        return safety;
    }

    public Integer getNumberOfDoor() {
        return numberOfDoor;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private CarClass carClass;
        private Size bootSize;
        private Price cost;
        private Price price;
        private Safety safety;
        private Integer numberOfDoor;
        private Integer capacity;

        public Builder withCarClass(String carClass) {
            if (carClass == null) {
                this.carClass = null;
            } else {
                this.carClass = CarClass.valueOf(carClass);
            }
            return this;
        }

        public Builder withBootSize(String bootSize) {
            if (bootSize == null) {
                this.bootSize = null;
            } else {
                this.bootSize = Size.valueOf(bootSize);
            }
            return this;
        }

        public Builder withCost(String cost) {
            if (cost == null) {
                this.cost = null;
            } else {
                this.cost = Price.valueOf(cost);
            }
            return this;
        }

        public Builder withPrice(String price) {
            if (price == null) {
                this.price = null;
            } else {
                this.price = Price.valueOf(price);
            }
            return this;
        }

        public Builder withSafety(String safety) {
            if (safety == null) {
                this.safety = null;
            } else {
                this.safety = Safety.valueOf(safety);
            }
            return this;
        }

        public Builder withNumberOfDoor(String numberOfDoor) {
            if (numberOfDoor == null) {
                this.numberOfDoor = null;
            } else if (numberOfDoor.equals("5more")) {
                this.numberOfDoor = 5;
            } else {
                this.numberOfDoor = Integer.valueOf(numberOfDoor);
            }
            return this;
        }

        public Builder withCapacity(String capacity) {
            if (capacity == null) {
                this.capacity = null;
            } else if (capacity.equals("more")) {
                this.capacity = 5;
            } else {
                this.capacity = Integer.valueOf(capacity);
            }

            return this;
        }


        public Car build() {
            return new Car(this);
        }
    }
}
