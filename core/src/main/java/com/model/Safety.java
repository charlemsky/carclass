package com.model;

import java.math.BigDecimal;

public enum Safety {
low(1), med(2), high(3);

    int value;

    Safety(int p) {
        value = p;
    }

    public int getValue() {
        return value;
    }

    public BigDecimal getNormalizeValue() {
        return new BigDecimal((double) value / 3d);
    }
    public static Safety fieldByValue(int value) {
        for (Safety v : values()) {
            if (v.value == value) {
                return v;
            }
        }
        throw new IllegalArgumentException(
                "No enum const " + Safety.class + "@value." + value);
    }
}
