package com.model;


import java.math.BigDecimal;

public enum Size {
    small(1), med(2), big(3);

    int value;

    Size(int p) {
        value = p;
    }

    public BigDecimal getNormalizeValue() {
        return new BigDecimal((double) value / 3d);
    }

    public int getValue() {
        return value;
    }

    public static Size fieldByValue(int value) {
        for (Size v : values()) {
            if (v.value == value) {
                return v;
            }
        }
        throw new IllegalArgumentException(
                "No enum const " + Size.class + "@value." + value);
    }
}
