package com.model;

import java.math.BigDecimal;

public class Normalize {

    public static BigDecimal normalizeCapacity(Integer value) throws IllegalAccessException {
        switch (value) {
            case 2 : return new BigDecimal((double)1/3d);
            case 4 : return new BigDecimal((double)2/3d);
            case 5 : return new BigDecimal(1);
            default : throw new IllegalAccessException("value not supported");
        }
    }

    public static BigDecimal normalizeNoOfDoor(Integer value) throws IllegalAccessException {
        switch (value) {
            case 2 : return new BigDecimal((double)1/4d);
            case 3 : return new BigDecimal((double)2/4d);
            case 4 : return new BigDecimal((double)3/4d);
            case 5 : return new BigDecimal(1);
            default : throw new IllegalAccessException("value not supported");
        }
    }
}
