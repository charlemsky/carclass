package com.model;

import java.math.BigDecimal;

public enum Price {
    vhigh(4), high(3), med(2), low(1);

    int value;

    Price(int p) {
        value = p;
    }

    public int getValue() {
        return value;
    }

    public BigDecimal getNormalizeValue() {
        return new BigDecimal((double) value / 4d);
    }

    public static Price fieldByValue(int value) {
        for (Price v : values()) {
            if (v.value == value) {
                return v;
            }
        }
        throw new IllegalArgumentException(
                "No enum const " + Price.class + "@value." + value);
    }
}
