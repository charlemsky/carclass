package com;

import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;


public class Reader {

    public static List<CarBean> readFile(final String fileName) throws Exception {
        ICsvBeanReader beanReader = null;
        List<CarBean> elementList = new ArrayList<CarBean>();
        try {
            beanReader = new CsvBeanReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);
            final String[] header =  beanReader.getHeader(true);
            final CellProcessor[] processors = getProcessors();
            CarBean bean = null;
            while( (bean =  beanReader.read(CarBean.class , header, processors)) != null ) {
                elementList.add(bean);
            }
        } finally {
            if (beanReader != null) {
                beanReader.close();
            }
        }
        return elementList;
    }

        private static CellProcessor[] getProcessors() {

            final CellProcessor[] processors = new CellProcessor[] {
                    new NotNull(),
                    new NotNull(),
                    new NotNull(),
                    new NotNull(),
                    new NotNull(),
                    new NotNull(),
                    new NotNull(),


            };
            return processors;
        }

}
