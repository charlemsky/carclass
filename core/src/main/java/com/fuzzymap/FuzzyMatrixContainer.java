package com.fuzzymap;

import java.math.BigDecimal;
import java.util.HashMap;

public class FuzzyMatrixContainer {

    public static final int matrixSize = 6;

    private HashMap<String, BigDecimal[][]> matrix;

    private int iteration;

    public FuzzyMatrixContainer() {
        this.matrix = new HashMap<>();
    }

    public void saveMatrix(BigDecimal[][]array, String name) {
        matrix.put(name, array);
    }

    public BigDecimal[][] getMatrix(String name) {
        return matrix.get(name)== null ? newMatrix(name) : matrix.get(name);
    }

    private BigDecimal[][] newMatrix(String name) {
        BigDecimal[][] array = new BigDecimal[matrixSize][matrixSize];
        for(int i =0; i<matrixSize; i++) {
            for(int j = 0; j < matrixSize; j++) {
                array[i][j] = new BigDecimal(0);
            }
        }
        matrix.put(name, array);
        return array;
    }

    private String checkClasifiacation(BigDecimal[] vectorData) {
        return  "not implemented";
    }

}
