package com.fuzzymap.utils;

import com.sun.istack.internal.NotNull;

import java.math.BigDecimal;

public class Adaptations {

    public static final BigDecimal a = new BigDecimal(1);

    private static BigDecimal sensitivtyFunction(@NotNull BigDecimal oldValue) {
        return new BigDecimal(0);
    }
    public static BigDecimal adaptationWeight(@NotNull BigDecimal oldValue) {
        return new BigDecimal(0);
    }

    public static BigDecimal hebbianLawLearning(BigDecimal oldValue,
                                                BigDecimal input, BigDecimal input2) {
        //oldValue + a ( input * input2 - oldValue)
        return oldValue.add(a.multiply(input.multiply(input2).subtract(oldValue)));
    }


}
