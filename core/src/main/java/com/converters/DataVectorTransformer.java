package com.converters;

import com.fuzzymap.FuzzyMatrixContainer;
import com.model.Car;
import com.model.Normalize;
import lombok.extern.log4j.Log4j;

import java.math.BigDecimal;
import java.util.function.Function;

public class DataVectorTransformer implements Function<Car, BigDecimal[]> {

    @Override
    public BigDecimal[] apply(Car car) throws IllegalArgumentException {
        BigDecimal[] vector = new BigDecimal[FuzzyMatrixContainer.matrixSize];
        vector[0] = car.getBootSize().getNormalizeValue();
        try {
            vector[1] = Normalize.normalizeCapacity(car.getCapacity());
            vector[2] = Normalize.normalizeNoOfDoor(car.getNumberOfDoor());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        vector[3] = car.getCostOfUse().getNormalizeValue();
        vector[4] = car.getPrice().getNormalizeValue();
        vector[5] = car.getSafety().getNormalizeValue();
        return vector;
    }
}
