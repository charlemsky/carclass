package com.converters;

import com.CarBean;
import com.model.Car;

import java.util.function.Function;

public class CarBeanTransformer implements com.google.common.base.Function<CarBean, Car> {

    public Car apply(CarBean carBean) {
        return Car.newBuilder()
                .withBootSize(carBean.getBootSize())
                .withCapacity(carBean.getCapacity())
                .withCost(carBean.getCostOfUse())
                .withCarClass(carBean.getCarClass())
                .withNumberOfDoor(carBean.getNumberOfDoor())
                .withPrice(carBean.getPrice())
                .withSafety(carBean.getSafety())
                .build();
    }
}
