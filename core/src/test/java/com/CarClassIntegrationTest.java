package com;

import com.converters.CarBeanTransformer;
import com.model.Car;
import com.validation.CarValidator;
import com.validation.IValidator;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class CarClassIntegrationTest {

    private static String FILE_PATH;

    @BeforeClass
    public static void setUp() throws URISyntaxException {
        FILE_PATH = Paths.get(ReaderTest.class.getResource("/exampleFile.csv").toURI()).toString();
    }

    @Test
    public void test() throws Exception {
        List<CarBean> carBeanList = Reader.readFile(FILE_PATH);
        IValidator validator = new CarValidator(carBeanList);
        boolean isValid = validator.validate();
        assertTrue(isValid);

        BadFieldGenerator badFieldGenerator = new BadFieldGenerator();
        carBeanList = badFieldGenerator.generate(carBeanList, 10);
        validator = new CarValidator(carBeanList);
        isValid = validator.validate();
        assertTrue(!isValid);


        DataRepairer repairer = new DataRepairer();
        carBeanList = repairer.repair(carBeanList, validator.getInvalidRecords());
        validator = new CarValidator(carBeanList);
        isValid = validator.validate();
        assertTrue(isValid);

        List<Car> cars = carBeanList
                .stream()
                .map(new CarBeanTransformer())
                .collect(Collectors.toList());

        assertTrue(cars.size() == carBeanList.size());

    }
}
