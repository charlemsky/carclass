package com;

import com.validation.CarValidator;
import com.validation.IValidator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class BadFieldGeneratorTest {



    private BadFieldGenerator generator = Mockito.mock(BadFieldGenerator.class);

    @Before
    public void setUp() {
        Mockito.when(generator.getRandomCarField()).thenReturn(CarField.BOOT);
        Mockito.when(generator.getRandomRecordNumber(Mockito.anyList())).thenReturn(0);
        Mockito.when(generator.generate(Mockito.anyList(), Mockito.any(Integer.class))).thenCallRealMethod();
    }



    @Test
    @SuppressWarnings("unchecked")
    public void shouldGenerateFakeFieldRecord() throws Exception {
        List<CarBean> fakeCarBean = generator.generate(mockGoodRecords(), 1 );
        IValidator validator = new CarValidator(fakeCarBean);
        assertTrue(!validator.validate());
        assertTrue(fakeCarBean.get(0).getBootSize().equals("wrongBootSize"));
    }


    private List<CarBean> mockGoodRecords() {
        CarBean carBean = new CarBean();
        carBean.setBootSize("small");
        carBean.setCapacity("4");
        carBean.setCarClass("acc");
        carBean.setCostOfUse("high");
        carBean.setSafety("med");
        carBean.setPrice("med");
        carBean.setNumberOfDoor("3");
        List<CarBean> list = new ArrayList<>();
        list.add(carBean);
        return list;

    }

}
