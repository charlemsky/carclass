package com.fuzzymap;

import com.*;
import com.converters.CarBeanTransformer;
import com.converters.DataVectorTransformer;
import com.fuzzymap.utils.Adaptations;
import com.model.Car;
import com.model.CarClass;
import com.validation.CarValidator;
import com.validation.IValidator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class FuzzyMapIntegrationTest {

    private static String FILE_PATH;
    private static int packageSize;
    private FuzzyMatrixContainer container;

    @BeforeClass
    public static void setUp() throws URISyntaxException {
        packageSize = 50;
        FILE_PATH = Paths.get(ReaderTest.class.getResource("/learnData/car.csv").toURI()).toString();
    }
    @Before
    public void init() {
        container = new FuzzyMatrixContainer();
    }


    @Test
    public void test() throws Exception {
        List<Car> carList = readDataFromFile();
        HashMap<CarClass, List<BigDecimal[]>> vectorMaps = new HashMap<>();
        vectorMaps.put(CarClass.unacc, getVectorData(carList, CarClass.unacc));
        vectorMaps.put(CarClass.acc, getVectorData(carList, CarClass.acc));
        vectorMaps.put(CarClass.good, getVectorData(carList, CarClass.good));
        vectorMaps.put(CarClass.vgood, getVectorData(carList, CarClass.vgood));


        for (Map.Entry<CarClass, List<BigDecimal[]>> entry : vectorMaps.entrySet()) {
            learnMap(entry.getValue(), entry.getKey());
        }


        assertTrue(true);

    }

    private void learnMap(List<BigDecimal[]> vectorList, CarClass carClass) {
        BigDecimal[][] fuzzyMatrix = container.getMatrix(carClass.name());
        for (BigDecimal[] element: vectorList) {
            for(int i = 0; i < 5; i++) {
                for(int j = 0; j < 5; j++) {
                    if(i==j)
                        continue;
                    fuzzyMatrix[i][j] = Adaptations.hebbianLawLearning(fuzzyMatrix[i][j], element[i], element[j]);
                }
            }
        }

    }
    private List<Car> readDataFromFile() throws Exception {
        List<CarBean> carBeanList = Reader.readFile(FILE_PATH);

        IValidator validator = new CarValidator(carBeanList);
        boolean isValid = validator.validate();
        assertTrue(isValid);
        return carBeanList
                .stream()
                .map(new CarBeanTransformer())
                .collect(Collectors.toList());
    }


    private List<Car> filterDataByClass(List<Car> cars, CarClass carClass) {
                return cars
                .stream()
                .filter(car -> car.getCarClass().equals(carClass))
                .collect(Collectors.toList());
    }


    private List<BigDecimal[]> getVectorData(List<Car> cars, CarClass carClass) {
        List<Car> carList = filterDataByClass(cars, carClass);
        return carList
                .stream()
                .map(new DataVectorTransformer())
                .limit(packageSize)
                .collect(Collectors.toList());

    }


}
