package com.fuzzymap;

import com.fuzzymap.utils.Adaptations;
import org.junit.Test;

import java.math.BigDecimal;

public class FuzzyMatrixContainerTest {


    @Test
    public void shouldCreateNewMatrix() {
        FuzzyMatrixContainer container = new FuzzyMatrixContainer();
        container.getMatrix("good");
        container.getMatrix("bad");
    }

    @Test
    public void shouldLearn() {
        FuzzyMatrixContainer container = new FuzzyMatrixContainer();
        BigDecimal[][] matrix = container.getMatrix("good");
        BigDecimal[] learnData = FuzzyUtils.getLearningVectro();
        for(int i = 0; i < 5; i++) {
            for(int j = 0; j < 5; j++) {
                if(i==j)
                    continue;
                matrix[i][j] = Adaptations.hebbianLawLearning(matrix[i][j], learnData[i], learnData[j]);
            }
        }
        container.saveMatrix(matrix, "good");
    }
}
