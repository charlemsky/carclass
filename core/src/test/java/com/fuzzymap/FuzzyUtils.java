package com.fuzzymap;

import java.math.BigDecimal;

public class FuzzyUtils {

    public static BigDecimal[] getLearningVectro() {
        BigDecimal[] vector = new BigDecimal[5];
        vector[0] = new BigDecimal(0.5);
        vector[1] = new BigDecimal(1);
        vector[2] = new BigDecimal(0.5);
        vector[3] = new BigDecimal(0.25);
        vector[4] = new BigDecimal(0.25);
        vector[5] = new BigDecimal(0.75);
        return vector;
    }
}
