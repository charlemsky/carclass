package com;

import com.validation.CarValidator;
import com.validation.IValidator;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertTrue;

public class DataRepairerTest {

    private List<CarBean> badRecords;

    @Before
    public void setUp() {
        this.badRecords = mockBadRecords();
    }



    @Test
    @SuppressWarnings("unchecked")
    public void shouldValidateAndRepairedBadRecords() throws Exception {
        DataRepairer repairer = new DataRepairer();
        IValidator validator = new CarValidator(badRecords);
        boolean isValid = validator.validate();
        assertTrue(!isValid);
        Properties invalidRecords = validator.getInvalidRecords();
        assertTrue(invalidRecords.size() == 1);
        badRecords = repairer.repair(badRecords, invalidRecords);
        validator = new CarValidator(badRecords);
        isValid = validator.validate();
        assertTrue(isValid);

    }


    private List<CarBean> mockBadRecords() {
        CarBean carBean = new CarBean();
        carBean.setBootSize("fsdf");
        carBean.setCapacity("4");
        carBean.setCarClass("fsdf");
        carBean.setCostOfUse("ffsdf");
        carBean.setSafety("med");
        carBean.setPrice("med");
        carBean.setNumberOfDoor("3");
        List<CarBean> list = new ArrayList<>();
        list.add(carBean);
        return list;
    }
}
