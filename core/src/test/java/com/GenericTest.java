package com;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GenericTest {

    @Test(expected = ClassCastException.class)
    @SuppressWarnings("unchecked")
    public void nonGenericList() {
        List list = new ArrayList();
        list.add("1");
        list.add(2);
        String element = (String) list.get(1); //ClassCastException
    }

    @Test()
    public void genericList() {
        List<String> list = new ArrayList<>();
        list.add("1");
//        list.add(2); błąd komilacji
        String element = list.get(0);
    }

}