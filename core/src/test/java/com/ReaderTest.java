package com;

import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;
import static org.junit.Assert.*;
public class ReaderTest {

    private static String FILE_PATH;

    @BeforeClass
    public static void setUp() throws URISyntaxException {
        FILE_PATH = Paths.get(ReaderTest.class.getResource("/exampleFile.csv").toURI()).toString();
    }

    @Test
    public void shouldReturnCarList() throws Exception {
        List<CarBean> carBeanList = Reader.readFile(FILE_PATH);
        assertTrue(carBeanList.size() > 0);
    }

}
