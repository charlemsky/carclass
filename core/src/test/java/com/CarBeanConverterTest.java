package com;

import com.converters.CarBeanTransformer;
import com.model.Car;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class CarBeanConverterTest {

    private List<CarBean> goodRecords;

    @Before
    public void setUp() {
        this.goodRecords = Arrays.asList(mockGoodRecord());
    }

    @Test
    public void shouldConvertGoodRecords() throws Exception {
        List<Car> resultList = goodRecords
                .stream()
                .map(new CarBeanTransformer())
                .collect(Collectors.toList());
        assertTrue(resultList.size() == 1);
    }


    private CarBean mockGoodRecord() {
        CarBean carBean = new CarBean();
        carBean.setBootSize("small");
        carBean.setCapacity("4");
        carBean.setCarClass("acc");
        carBean.setCostOfUse("high");
        carBean.setSafety("med");
        carBean.setPrice("med");
        carBean.setNumberOfDoor("3");
        return carBean;
    }

}
