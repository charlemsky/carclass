package com;

import com.validation.CarValidator;
import com.validation.IValidator;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

public class ValidatorTest {

    private List<CarBean> goodRecords;
    private List<CarBean> badRecords;

    @Before
    public void setUp() {
        this.goodRecords = mockGoodRecords();
        this.badRecords = mockBadRecords();
    }

    @Test
    public void shouldValidateGoodRecords() throws Exception {
        IValidator validator = new CarValidator(goodRecords);
        boolean isValid = validator.validate();
        assertTrue(isValid);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldValidateBadRecords() throws Exception {
        IValidator validator = new CarValidator(badRecords);
        boolean isValid = validator.validate();
        assertTrue(!isValid);

        Properties invalidRecords = validator.getInvalidRecords();
        assertTrue(invalidRecords.size() == 1);

        List<CarField> carFields = (List<CarField>) invalidRecords.get(11);

        assertTrue(carFields.contains(CarField.BOOT));
        assertTrue(carFields.contains(CarField.CLASS));
        assertTrue(carFields.contains(CarField.COST));
    }

    private List<CarBean> mockBadRecords() {
        List<CarBean> carBeanList = new ArrayList<>();
        for (int i=0;i<10;i++) {
            carBeanList.add(mockGoodRecord());
        }
        carBeanList.add(mockBadRecord());
        return carBeanList;
    }

    private List<CarBean> mockGoodRecords() {
        List<CarBean> carBeanList = new ArrayList<>();
        for (int i=0;i<10;i++) {
            carBeanList.add(mockGoodRecord());
        }
        return carBeanList;
    }

    private CarBean mockGoodRecord() {
        CarBean carBean = new CarBean();
        carBean.setBootSize("small");
        carBean.setCapacity("4");
        carBean.setCarClass("acc");
        carBean.setCostOfUse("high");
        carBean.setSafety("med");
        carBean.setPrice("med");
        carBean.setNumberOfDoor("3");
        return carBean;
    }

    private CarBean mockBadRecord() {
        CarBean carBean = new CarBean();
        carBean.setBootSize("fsdf");
        carBean.setCapacity("4");
        carBean.setCarClass("fsdf");
        carBean.setCostOfUse("ffsdf");
        carBean.setSafety("med");
        carBean.setPrice("med");
        carBean.setNumberOfDoor("3");
        return carBean;
    }
}
